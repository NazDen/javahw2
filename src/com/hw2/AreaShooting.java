package com.hw2;

import java.util.Random;
import java.util.Scanner;


public class AreaShooting {
    public static void main(String[] args) {

        char[][] area = createArea();

        int randRow;
        int randCol;
        Random random = new Random();


        boolean direction = random.nextBoolean();
        if (direction) {
            randRow = random.nextInt(5) + 1;
            randCol = random.nextInt(3) + 1;
        } else {
            randRow = random.nextInt(3) + 1;
            randCol = random.nextInt(5) + 1;
        }

        System.out.println("All set. Get ready to rumble!");

        int rowNum = setRowNum();
        int columnNum = setColumnNum();


        if (direction) {
            while (area[randRow][randCol] != 'x' || area[randRow][randCol + 1] != 'x' || area[randRow][randCol + 2] != 'x') {
                if ((randRow == rowNum && randCol == columnNum) || (randRow == rowNum &&(randCol + 1) == columnNum) || (randRow == rowNum &&(randCol + 2) == columnNum)) {

                    System.out.println("You hit!");
                    area[rowNum][columnNum] = 'x';
                    showArea(area);

                    if (area[randRow][randCol] != 'x' || area[randRow][randCol + 1] != 'x' || area[randRow][randCol + 2] != 'x') {

                        rowNum = setRowNum();
                        columnNum = setColumnNum();
                    }
                } else {

                    area[rowNum][columnNum] = '*';
                    showArea(area);
                    rowNum = setRowNum();
                    columnNum = setColumnNum();
                }
            }
        } else {
            while (area[randRow][randCol] != 'x' || area[randRow + 1][randCol] != 'x' || area[randRow + 2][randCol] != 'x') {
                if ((randRow == rowNum&&randCol == columnNum) || ((randRow + 1) == rowNum&&randCol == columnNum) || ((randRow + 2) == rowNum && randCol == columnNum)) {

                    System.out.println("You hit!");
                    area[rowNum][columnNum] = 'x';
                    showArea(area);

                    if (area[randRow][randCol] != 'x' || area[randRow + 1][randCol] != 'x' || area[randRow + 2][randCol] != 'x') {

                        rowNum = setRowNum();
                        columnNum = setColumnNum();
                    }
                }

            else{

                area[rowNum][columnNum] = '*';
                showArea(area);
                rowNum = setRowNum();
                columnNum = setColumnNum();
            }
        }
    }


        System.out.println("You have won!");
}


    static char[][] createArea() {
        char[][] area = new char[6][6];

        for (int i = 0; i < area.length; i++) {
            int n = 48;

            if (i == 0) {
                for (int j = 0; j < area[i].length; j++) {
                    area[i][j] = (char) n;
                    System.out.print(" | " + area[i][j]);
                    n++;
                }
            } else {
                for (int j = 0; j < area[i].length; j++) {

                    n+= i;
                    if (j == 0) {
                        area[i][j] = (char) n;
                        System.out.print(" | " + area[i][j]);
                    } else {
                        area[i][j] = '-';
                        System.out.print(" | " + area[i][j]);
                    }
                }
            }
            System.out.println(" |");
        }
        return area;
    }

    static void showArea(char[][] area) {
        for (char row[] : area) {
            for (char column : row) {
                System.out.print(" | " + column);
            }
            System.out.println(" |");
        }
    }

    static int setRowNum() {
        System.out.println("Enter the row number from 1 to 5:");
        Scanner row = new Scanner(System.in);
        String rowStr = row.next();
        while (!rowStr.matches("[1-5]")) {
            System.out.println("Your input doesn't suit any row. Enter a valid number from 1 to 5.");
            rowStr = row.next();
        }
        return Integer.parseInt(rowStr);
    }


    static int setColumnNum() {
        System.out.println("Enter the column number from 1 to 5:");
        Scanner column = new Scanner(System.in);
        String columnStr = column.nextLine();
        while (!columnStr.matches("[1-5]")) {
            System.out.println("Your input doesn't suit any column. Enter a valid number from 1 to 5.");
            columnStr = column.nextLine();
        }
        return Integer.parseInt(columnStr);
    }

}